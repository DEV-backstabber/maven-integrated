package us.hhbz.fyp;

import us.hhbz.fyp.guis.AbstractGui;
import us.hhbz.fyp.guis.DataScreen;
import us.hhbz.fyp.guis.LoadingScreen;
import us.hhbz.fyp.managers.AudioManager;
import us.hhbz.fyp.managers.ConditionManager;
import us.hhbz.fyp.managers.MySqlManager;
import us.hhbz.fyp.managers.ObdManager;
/**
 * this is the Objectified main class
 * Naturally this will be a singleton class (meaning it will have only 1 instance)
 * The idea for using objectified main is so dependency injection can be used for all utility classes
 * @author Abdul Hadi
 *
 */
public class MainObject {

	/**
	 * declaring all utility classes as objects
	 */
	private AbstractGui runningGui;
	private AudioManager audioManager;
	private ObdManager obdManager;
	private ConditionManager conditions;
	private MySqlManager sqlManager;
	
	/**
	 * this is the (objectified) entry point for this program
	 */
	public void onStart() {
		audioManager=new AudioManager(this);
		obdManager=new ObdManager(this);
		obdManager.setup();
		conditions=new ConditionManager(this);
		conditions.setup();
		sqlManager=new MySqlManager(this);
		sqlManager.setup();
		
		runningGui=new LoadingScreen();
		runningGui.constructGui();
		runningGui.displayGui();
		for(int progress=0;progress<=100;progress++) {
			runningGui.setProgress(progress);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		runningGui.hideGui();
		runningGui=new DataScreen();
		runningGui.constructGui();
		runningGui.displayGui();
	}
	
	/**
	 * methods to get the instance of all utilities
	 * these methods are used in utility classes through
	 * dependency injection.
	 * @return instance of the requested utility
	 */
	public AbstractGui getRunningGui() {
		return this.runningGui;
	}
	public ObdManager getObdManager() {
		return this.obdManager;
	}
	public AudioManager getAudioManager() {
		return this.audioManager;
	}
}
