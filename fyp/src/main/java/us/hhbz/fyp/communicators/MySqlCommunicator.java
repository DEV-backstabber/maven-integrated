package us.hhbz.fyp.communicators;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import us.hhbz.fyp.ProjectMain;
import us.hhbz.fyp.datastores.CarData;
import us.hhbz.fyp.enums.DataTypes;

/**
 * This is one of the most important & complex communicator in this project
 * It Communicated between the mysql database & out code
 * the main reason for this is so that our mobile application  (if completed)
 * can also hook up to that database & fetch data for our vehicle
 * @author Abdul Hadi
 *
 */


public class MySqlCommunicator {
	
	private String host;
	private String port;
	private String database;
	private String username;
	private String password;
	private boolean isUseable=false;
	
	private Connection connection;

	/**
	 * create a valid connection to the database
	 * @param host
	 * @param port
	 * @param database
	 * @param username
	 * @param password
	 */
	public void create(String host,String port,String database,String username,String password) {
		this.host=host;
		this.port=port;
		this.database=database;
		this.username=username;
		this.password=password;
		openConnection();
		createTable();
	}
	/**
	 * check if the communicator is connected properly
	 * @return
	 */
	public boolean isUseable() {
		return this.isUseable;
	}
	/**
	 * Updates the database for the given Data & DataType
	 * @param type of the data
	 * @param the actual data
	 */
	public void updateData(DataTypes type,int data) {
		if(!isUseable)
			openConnection();
		if(isCarCreated()) {
			String statement="UPDATE `carData` SET `"+type.getCodeName()+"` = "+data+" WHERE `carData`.`carId` = '"+ProjectMain.CAR_ID+"'";
			try {
				connection.createStatement().execute(statement);
			} catch (SQLException e) {
				System.out.println("[My-Sql] Error couldnt update database for "+type.name());
				e.printStackTrace();
			}
		}
		else {
			String statement="INSERT INTO `clans` (`carId`,`"+type.getCodeName()+"`) VALUES ( '"+ProjectMain.CAR_ID+"' ,"+data+")";
			try {
				connection.createStatement().execute(statement);
			} catch (SQLException e) {
				System.out.println("[My-Sql] Error couldnt update database for "+type.name());
				e.printStackTrace();
			}
		}
	}
	/**
	 * Updates the database for all the data of the vehicle
	 * @param all the data packaged inside CarData object
	 */
	public void updateAll(CarData data) {
		
		try {
			connection.createStatement().execute("DELETE FROM `carData` WHERE `carData`.`carId` = '"+ProjectMain.CAR_ID+"'");
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		String columns="carId";
		String values="'"+ProjectMain.CAR_ID+"'";
		for(DataTypes type:DataTypes.values()) {
			columns=columns+","+type.getCodeName();
			values=values+","+data.getData(type);
		}
		String statement="INSERT INTO `carData` ("+columns+") VALUES ("+values+")";
		try {
			connection.createStatement().execute(statement);
		} catch (SQLException e) {
			System.out.println("[My-Sql] Error couldnt update database.");
			e.printStackTrace();
		}
	}
	/**
	 * Fetch a DataType from the database if it exists
	 * @param the type of data to get
	 * @return the data
	 */
	public int getData(DataTypes type) {
		if(!isUseable)
			openConnection();
		if(!isCarCreated())
			return -1;
		try {
			ResultSet r = connection
		        .createStatement()
		        .executeQuery(
		          "SELECT * FROM `carData` WHERE carId = '" + 
		        		  ProjectMain.CAR_ID + "'");
			if (r.next()) 
			{
				return r.getInt(type.getCodeName());
			}
			r.close();
		} 
	    catch (SQLException e) 
	    {
			System.out.println("[My-Sql] Error couldnt fetch data "+type.name());
	    	e.printStackTrace();
	    } 
		return -1;
	}
	/**
	 * Fetch all data packaged inside CarData object
	 * if it exists
	 * @return the data
	 */
	public CarData getAllData() {
		if(!isUseable)
			openConnection();
		if(!isCarCreated())
			return null;
		try {
			ResultSet r = connection
		        .createStatement()
		        .executeQuery(
		          "SELECT * FROM `carData` WHERE carId = '" + 
		        		  ProjectMain.CAR_ID + "'");
			if (r.next()) 
			{
				CarData data=new CarData();
				for(DataTypes type:DataTypes.values()) {
					data.setData(type, r.getInt(type.getCodeName()));
				}
				return data;
			}
			r.close();
		} 
	    catch (SQLException e) 
	    {
			System.out.println("[My-Sql] Error couldnt fetch Car Data.");
	    	e.printStackTrace();
	    } 
		return null;
	}
	/**
	 * Check if the table for this car id was created in our database
	 * @return
	 */
	public boolean isCarCreated() {
		try {
			ResultSet r = connection
		        .createStatement()
		        .executeQuery(
		          "SELECT * FROM `carData` WHERE carId = '" + 
		        		  ProjectMain.CAR_ID + "'");
		      boolean contains = r.next();
		      r.close();
		      return contains;
		    } 
		    catch (SQLException|NullPointerException e) 
		    {
		      e.printStackTrace();
		      return false;
		    } 
	}
	/**
	 * Creates a table for our use if it doesnt already exist
	 */
	public void createTable() {
		if(!isUseable)
			openConnection();
		String statement="CREATE TABLE IF NOT EXISTS `carData` (\n"
				+ "`carId` varchar(36) NOT NULL, \n"
				+ "`"+DataTypes.RPM.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.SPEED.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.COOLANT_TEMPERATURE.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.INTAKE_TEMPERATURE.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.ENGINE_TEMPERATURE.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.FUEL_PRESSURE.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.INTAKE_PRESSURE.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.DISTANCE_MIL.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.DISTANCE_CC.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.INJECTION_TIME.getCodeName()+"` int(11) ,\n"
				+ "`"+DataTypes.TORGUE_PERCENTAGE.getCodeName()+"` int(11) ,\n"
				+ "PRIMARY KEY  (`carId`)\n)";
		try {
			connection.createStatement().execute(statement);
		} 
		catch (SQLException e) {
			System.out.println("[My-Sql] Error couldnt create table \"carData\".");
			e.printStackTrace();
		} 
	}
	/**
	 * opens a connection to the database fr us to use
	 */
	public void openConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	    try {
	    	connection = DriverManager.getConnection("jdbc:mysql://" + 
	          host + ":" + 
	          port + "/" + 
	          database, 
	          username, 
	          password);
	    	isUseable=true;
	    } 
	    catch (SQLException |NullPointerException e) {
			System.out.println("[My-Sql] Error couldnt connect to database.");
	      e.printStackTrace();
	      isUseable=false;
	    }
	}
	/**
	 * Closes the connection to prevent data leaks
	 */
	public void closeConnection() {
		try {
	      if (!connection.isClosed() || connection != null)
	        connection.close(); 
	    } 
		catch (SQLException e) {
	      e.printStackTrace();
	    } 
		isUseable=false;
	}
}
