package us.hhbz.fyp.communicators;

import java.io.IOException;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.StopBits;
/**
 * An implementation of serial port communication 
 * which sends & recieves data from arduino using the
 * usb port on raspberry pi & arduino
 * 
 *  This code makes use of the pi4j Project found at
 *  https://pi4j.com/1.2/index.html
 *  Basically it provides a library useable in java for manipulating
 *  raspberry pi beyond java limitations
 * @author Abdul Hadi
 *
 */
public class ArduinoCommunicator {
	private Serial port;
	private String lastData;
	/**
	 * Creates an instance of the communicator
	 * Since more than one device can be connected to a Pi
	 * we need the device id of our arduino
	 * commonly arduino's ids are "/dev/ttyACM" with a number at the end
	 * ranging from 0-9 if more than 1 arduino is connected
	 * @param device
	 */
	public ArduinoCommunicator(String device) {
		this.port=SerialFactory.createInstance();
		
		SerialConfig config=new SerialConfig();
		config.device(device);
		config.baud(Baud._9600);
		config.dataBits(DataBits._8);
		config.parity(Parity.NONE);
		config.stopBits(StopBits._1);
		config.flowControl(FlowControl.NONE);
		try {
			port.open(config);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		port.addListener(new SerialDataEventListener() {
			
			@Override
			public void dataReceived(SerialDataEvent event) {
				try {
					lastData=event.getAsciiString();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Method to send serial message to an arduino
	 * same as doing Serial.print(data) in arduino IDE
	 * @param data
	 */
	public void sendData(String data) {
		try {
			port.write(data);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * method to get the last recieved data from the arduino
	 * since java uses eventhandling for recieving data from serial ports (alot faster)
	 * we are saving the last recieved data string in this class to send back 
	 * @return
	 */
	public String getLastData() {
		return lastData;
	}
}
