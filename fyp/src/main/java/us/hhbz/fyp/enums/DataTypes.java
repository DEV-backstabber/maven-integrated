package us.hhbz.fyp.enums;

/**
 * enumerator for all the types of data which can be recieved from
 * the obd slave
 * the enumerator also contains
 * 1)A simple easy to read name
 * 2)The coded name used in mysql database
 * 3)The command used for the obd slave
 * 4)upper & lower safe limits of the value
 * @author Abdul Hadi
 *
 */
public enum DataTypes {
	RPM("R.P.M","rpm","rpm",0,4000),
	SPEED("Speed","speed","speed",0,120),
	COOLANT_TEMPERATURE("Coolant Temp","coolant_temp","temp:coolant",4,100),
	INTAKE_TEMPERATURE("Intake Temp","intake_temp","temp:intake",4,100),
	ENGINE_TEMPERATURE("Engine Temp","engine_temp","temp:engine",4,100),
	INTAKE_PRESSURE("Intake Pressure","intake_pressure","pressure:intake",1,10),
	FUEL_PRESSURE("Fuel Pressure","fuel_pressure","pressure:fuel",1,10),
	DISTANCE_MIL("Distance Mil","distance_mil","distance:mil",0,1000),
	DISTANCE_CC("Distance CC","distance_cc","distance:cc",0,1000),
	INJECTION_TIME("Injection Time","injection_time","time:inject",1,10),
	TORGUE_PERCENTAGE("Torgue","torgue","torgue:per",10,80),;
	
	private String codeName;
	private String obdCommand;
	private String displayName;
	private int upperLimit;
	private int lowerLimit;
	DataTypes(String displayName,String codeName,String obdCommand,int lowerLimit,int upperLimit) {
		this.displayName=displayName;
		this.codeName=codeName;
		this.obdCommand=obdCommand;
		this.upperLimit=upperLimit;
		this.lowerLimit=lowerLimit;
	}
	public String getDisplayName( ) {
		return this.displayName;
	}
	public String getObdCommand() {
		return this.obdCommand;
	}
	public String getCodeName() {
		return this.codeName;
	}
	public boolean isInLimit(int value) {
		if(value>upperLimit)
			return false;
		if(value<lowerLimit)
			return false;
		return true;
	}
}
