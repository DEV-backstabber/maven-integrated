package us.hhbz.fyp.enums;
/**
 * An enumerator for all the audios stored inside out audio slave
 * it also stores the command arguments needed to send to the slave
 * @author Abdul Hadi
 *
 */
public enum AudioTypes {
	WELCOME_FINE("welcome_fine"),
	WELCOME_ERROR("welcome_error"),
	FAULT_RPM("fault_rpm"),
	FAULT_SPEED("fault_speed"),
	FAULT_COOLANT_HOT("fault_coolant_hot"),
	FAULT_INTAKE_HOT("fault_intake_hot"),
	FAULT_ENGINE_HOT("fault_engine_hot"),
	FAULT_FUEL("fault_fuel"),
	FINE("fine");
	
	private String slaveCommand;
	AudioTypes(String slaveCommand) {
		this.slaveCommand=slaveCommand;
	}
	public String getCommand() {
		return slaveCommand;
	}
}
