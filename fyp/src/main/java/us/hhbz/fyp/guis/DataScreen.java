package us.hhbz.fyp.guis;

import us.hhbz.fyp.guis.frames.ButtonFrame;


public class DataScreen extends AbstractGui {

	/**
	 * this creates the data screen gui
	 * with all the stats of out vehicle
	 */
	@Override
	public void constructGui() {
		this.guiName="dataScreen";
		frame=new ButtonFrame();
	    this.state=GuiState.HIDDEN;
	    this.buttons=((ButtonFrame)frame).getButtons();
	}
	/**
	 * this is blank since its not applicable
	 */
	@Override
	public void setProgress(int progress) {
		
	}

}
