package us.hhbz.fyp.guis.frames;

import java.awt.BorderLayout;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import us.hhbz.fyp.ProjectMain;
import us.hhbz.fyp.guis.AbstractGui;

import javax.swing.JSplitPane;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * This is an extension of the JFrame class
 * which creates a full loading screen gui instead
 * of a blank gui in creation.
 * this method saves alot of time & effort while making a new gui.
 * @author Abdul Hadi
 *
 */
@SuppressWarnings("serial")
public class LoadingFrame extends JFrame {

	private JProgressBar progressBar;
	public LoadingFrame() {
		
		setName("Fyp-Project");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setExtendedState(JFrame.MAXIMIZED_BOTH);
		setForeground(new Color(100, 149, 237));
		setBackground(new Color(95, 158, 160));
		setType(Type.UTILITY);
		
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setContinuousLayout(true);
		splitPane.setEnabled(false);
		splitPane.setResizeWeight(0.15);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setMinimum(0);
		progressBar.setMaximum(100);
		progressBar.setValue(0);
		splitPane.setLeftComponent(progressBar);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setContinuousLayout(true);
		splitPane_1.setEnabled(false);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setRightComponent(splitPane_1);
		
		JLabel lblLoadingProject = new JLabel("Loading Project");
		lblLoadingProject.setHorizontalAlignment(SwingConstants.CENTER);
		lblLoadingProject.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblLoadingProject.setBackground(Color.BLUE);
		splitPane_1.setLeftComponent(lblLoadingProject);
		
		
		InputStream stream=ProjectMain.getResource("team.png");
		BufferedImage image = null;
		try {
			image = ImageIO.read(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		image=AbstractGui.resize(image, 850, 850);
		JPanel panel = new ImagePane(image, true);
		splitPane_1.setRightComponent(panel);
	}
	public JProgressBar getProgressBar() {
		return this.progressBar;
	}
}
