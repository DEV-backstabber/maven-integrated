package us.hhbz.fyp.guis.frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import us.hhbz.fyp.ProjectMain;
import us.hhbz.fyp.builders.QrCodeBuilder;
import us.hhbz.fyp.enums.DataTypes;
import us.hhbz.fyp.guis.AbstractGui;

/**
 * This is an extension of the JFrame class
 * which creates a full stats screen gui instead
 * of a blank gui in creation.
 * this method saves alot of time & effort while making a new gui.
 * @author Abdul Hadi
 *
 */
@SuppressWarnings("serial")
public class ButtonFrame extends JFrame {
	private Map<DataTypes, JButton> buttons=new HashMap<DataTypes, JButton>();
	public ButtonFrame() {

		setName("Fyp-Project");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setExtendedState(JFrame.MAXIMIZED_BOTH);
		setForeground(new Color(100, 149, 237));
		setBackground(new Color(95, 158, 160));
		setType(Type.UTILITY);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.7);
		splitPane.setEnabled(false);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.33);
		splitPane_1.setEnabled(false);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setLeftComponent(splitPane_1);
		
		JSplitPane splitPane_2 = new JSplitPane();
		splitPane_2.setResizeWeight(0.5);
		splitPane_2.setEnabled(false);
		splitPane_2.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setRightComponent(splitPane_2);
		
		JSplitPane splitPane_5 = new JSplitPane();
		splitPane_5.setResizeWeight(0.34);
		splitPane_5.setEnabled(false);
		splitPane_2.setLeftComponent(splitPane_5);
		
		JSplitPane splitPane_6 = new JSplitPane();
		splitPane_6.setResizeWeight(0.57);
		splitPane_6.setEnabled(false);
		splitPane_5.setRightComponent(splitPane_6);
		
		JButton btnEngineTemp = new JButton(DataTypes.ENGINE_TEMPERATURE.getDisplayName());
		btnEngineTemp.setForeground(Color.WHITE);
		btnEngineTemp.setBackground(Color.BLUE);
		btnEngineTemp.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_6.setLeftComponent(btnEngineTemp);
		buttons.put(DataTypes.ENGINE_TEMPERATURE, btnEngineTemp);
		
		JButton btnFuel = new JButton(DataTypes.FUEL_PRESSURE.getDisplayName());
		btnFuel.setForeground(Color.WHITE);
		btnFuel.setBackground(Color.BLUE);
		btnFuel.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_6.setRightComponent(btnFuel);
		buttons.put(DataTypes.FUEL_PRESSURE, btnFuel);
		
		JButton btnIntake = new JButton(DataTypes.INTAKE_TEMPERATURE.getDisplayName());
		btnIntake.setForeground(Color.WHITE);
		btnIntake.setBackground(Color.BLUE);
		btnIntake.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_5.setLeftComponent(btnIntake);
		buttons.put(DataTypes.INTAKE_TEMPERATURE, btnIntake);
		
		JSplitPane splitPane_7 = new JSplitPane();
		splitPane_7.setResizeWeight(0.325);
		splitPane_7.setEnabled(false);
		splitPane_2.setRightComponent(splitPane_7);
		
		JSplitPane splitPane_8 = new JSplitPane();
		splitPane_8.setResizeWeight(0.53);
		splitPane_8.setEnabled(false);
		splitPane_7.setRightComponent(splitPane_8);
		
		JButton btnIntakePressure = new JButton(DataTypes.INTAKE_PRESSURE.getDisplayName());
		btnIntakePressure.setForeground(Color.WHITE);
		btnIntakePressure.setBackground(Color.BLUE);
		btnIntakePressure.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_8.setLeftComponent(btnIntakePressure);
		buttons.put(DataTypes.INTAKE_PRESSURE, btnIntakePressure);
		
		JButton btnTorgue = new JButton(DataTypes.TORGUE_PERCENTAGE.getDisplayName());
		btnTorgue.setForeground(Color.WHITE);
		btnTorgue.setBackground(Color.BLUE);
		btnTorgue.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_8.setRightComponent(btnTorgue);
		buttons.put(DataTypes.TORGUE_PERCENTAGE, btnTorgue);
		
		JButton btnInjection = new JButton(DataTypes.INJECTION_TIME.getDisplayName());
		btnInjection.setForeground(Color.WHITE);
		btnInjection.setBackground(Color.BLUE);
		btnInjection.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_7.setLeftComponent(btnInjection);
		buttons.put(DataTypes.INJECTION_TIME, btnInjection);
		
		JSplitPane splitPane_3 = new JSplitPane();
		splitPane_3.setResizeWeight(0.35);
		splitPane_3.setEnabled(false);
		splitPane_1.setLeftComponent(splitPane_3);
		
		JSplitPane splitPane_4 = new JSplitPane();
		splitPane_4.setResizeWeight(0.6);
		splitPane_4.setEnabled(false);
		splitPane_3.setRightComponent(splitPane_4);

		JButton btnRpm = new JButton(DataTypes.RPM.getDisplayName());
		btnRpm.setForeground(Color.WHITE);
		btnRpm.setBackground(Color.BLUE);
		btnRpm.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_4.setLeftComponent(btnRpm);
		buttons.put(DataTypes.RPM, btnRpm);
		
		JButton btnCoolant = new JButton(DataTypes.COOLANT_TEMPERATURE.getDisplayName());
		btnCoolant.setForeground(Color.WHITE);
		btnCoolant.setBackground(Color.BLUE);
		btnCoolant.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_4.setRightComponent(btnCoolant);
		buttons.put(DataTypes.COOLANT_TEMPERATURE, btnCoolant);
		
		JButton btnSpeed = new JButton(DataTypes.SPEED.getDisplayName());
		btnSpeed.setForeground(Color.WHITE);
		btnSpeed.setBackground(Color.BLUE);
		btnSpeed.setFont(new Font("Verdana", Font.ITALIC, 25));
		splitPane_3.setLeftComponent(btnSpeed);
		buttons.put(DataTypes.SPEED, btnSpeed);
		
		JSplitPane splitPane_9 = new JSplitPane();
		splitPane_9.setResizeWeight(0.5);
		splitPane.setRightComponent(splitPane_9);

		BufferedImage image = QrCodeBuilder.create("hhbz-fyp").addData("carId", ProjectMain.CAR_ID).getQrCode();
		image=AbstractGui.resize(image, 240 , 240);
		JPanel panel = new ImagePane(image, false);
		splitPane_9.setLeftComponent(panel);
		
		
		InputStream stream=ProjectMain.getResource("logo.jpg");
		BufferedImage logo = null;
		try {
			logo = ImageIO.read(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logo=AbstractGui.resize(logo, 240 , 240);
		

		JPanel panel_1 = new ImagePane(logo, false);
		splitPane_9.setRightComponent(panel_1);
	}
	
	public Map<DataTypes, JButton> getButtons() {
		return this.buttons;
	}

}
