package us.hhbz.fyp.guis;

import javax.swing.JProgressBar;
import us.hhbz.fyp.guis.frames.LoadingFrame;


public class LoadingScreen extends AbstractGui{

	private JProgressBar progressbar=new JProgressBar();
	/**
	 * this creates the loading screen gui
	 */
	@Override
	public void constructGui() {
		this.guiName="loadingScreen";
		frame = new LoadingFrame();
		this.state=GuiState.HIDDEN;
		this.progressbar=((LoadingFrame)frame).getProgressBar();
	}
	@Override
	public void setProgress(int progress) {
		while (progress>100) {
		   progress=progress-100;
		}
		progressbar.setValue(progress);
	}
}
