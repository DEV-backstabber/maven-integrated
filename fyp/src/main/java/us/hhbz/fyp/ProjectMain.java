package us.hhbz.fyp;


import java.io.InputStream;

/**
 * This is the starting point of out project
 * Some of the important data is stored here that can be
 * changed in each new module
 * 
 * @author Abdul Hadi
 *
 */
public class ProjectMain {

	/**
	 * static data for car id & my-SQL db which is linked to this project
	 * since we are using local database hence the hosts/ids are local
	 */
	public static String CAR_ID="brv_1";
	public static String MYSQL_HOST="localhost";
	public static String MYSQL_PORT="3306";
	public static String MYSQL_DATABASE="fyp";
	public static String MYSQL_USERNAME="test";
	public static String MYSQL_PASSWORD="test";
	
	/**
	 * entry point for the program.
	 * to properly implement oop principles an object of main is created 
	 * and called instead of having all code here
	 * @param args
	 */
	public static void main(String[] args) {  
		new MainObject().onStart();
	}  
	/**
	 * Simple method to fetch any resource (images)
	 * from the .jar file 
	 * @param the name of the file with extension (example logo.jpg)
	 * @return InputStream for the file if it exists
	 */
	public static InputStream getResource(String fileName) {
		
		return ProjectMain.class.getResourceAsStream("/media/"+fileName);
	      
	}
}
