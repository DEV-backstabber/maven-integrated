package us.hhbz.fyp.builders;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
/**
 * This is a hybrid qr-Code builder & decompiler
 * I call this hybrid since you can fetch data from a qr code as well
 * as build a qr code.
 * @author Abdul Hadi
 *
 */
public class QrCodeBuilder {
	private String header;
	private Map<String, String> data=new HashMap<String, String>();
	/**
	 * Static method to create a blank builder
	 * @return builder
	 */
	public static QrCodeBuilder create2() {
		QrCodeBuilder builder = new QrCodeBuilder();
		builder.header="";
		return builder;
	}
	/**
	 * Static method to create a builder with the given header
	 * @return builder
	 */
	public static QrCodeBuilder create(String header) {
		QrCodeBuilder builder = new QrCodeBuilder();
		builder.header=header;
		return builder;
	}
	/**
	 * Static method to create a builder from a qr-code image
	 * @return builder
	 */
	public static QrCodeBuilder decode(BufferedImage image) {
		QrCodeBuilder builder=new QrCodeBuilder();
		builder.read(image);
		return builder;
	}
	/**
	 * Get a/an (Buffered) Image from the builder
	 * @return image
	 */
	public BufferedImage getQrCode() {
		String code=header;
		for(String id:data.keySet()) {
			String part=id+"%"+data.get(id);
			code=code+":"+part;
		}
		return createQRImage(code);
	}
	/**
	 * set the header for the builder
	 * @param header
	 * @return builder
	 */
	public QrCodeBuilder setHeader(String header) {
		this.header=header;
		return this;
	}
	
	/**
	 * fetch the header (hybrid method)
	 * @return header
	 */
	public String getHeader() {
		return this.header;
	}
	/**
	 * Add a data against an id
	 * @param id for the data
	 * @param the actual data
	 * @return builder
	 */
	public QrCodeBuilder addData(String id,String data) {
		this.data.put(id, data);
		return this;
	}
	/**
	 * Fetch data against an id(if it exists)
	 * @param id for the data
	 * @return the data
	 */
	public String getData(String id) {
		return this.data.get(id);
	}
	/**
	 * Checks if it has an id
	 * @param id to check
	 * @return 
	 */
	public boolean hasData(String id) {
		return this.data.containsKey(id);
	}
	/**
	 * Read an image & decode data into header & id-data system
	 * @param image to read
	 */
	private void read(BufferedImage image) {
		String code=readQR(image);
		String[] chunks=code.split(":");
		this.header=chunks[0];
		for(int i=1;i<chunks.length;i++) {
			String chunk=chunks[i];
			if(chunk.split("%").length==2)
				this.data.put(chunk.split("%")[0], chunk.split("%")[1]);
		}
	}
	/**
	 * Generate a qr code from plain text
	 * @param text
	 * @return
	 */
	private BufferedImage createQRImage(String text) {
		
		Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<>();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix byteMatrix = null;
		try {
			byteMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, 180, 180, hintMap);
		} catch (WriterException e) {
			e.printStackTrace();
		}
		int matrixWidth = byteMatrix.getWidth();
		BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_RGB);
		image.createGraphics();

		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, matrixWidth, matrixWidth);
		graphics.setColor(Color.BLACK);

		for (int i = 0; i < matrixWidth; i++) {
			for (int j = 0; j < matrixWidth; j++) {
				if (byteMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}
		return image;
	}
	/**
	 * Get the plain text from a qr code
	 * @param image to read
	 * @return plain text
	 */
	private String readQR(BufferedImage image) 
    { 
        LuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        try {
            Result result = new MultiFormatReader().decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            return null;
        }
    } 
}
