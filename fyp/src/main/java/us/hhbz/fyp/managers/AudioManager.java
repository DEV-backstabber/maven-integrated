package us.hhbz.fyp.managers;

import us.hhbz.fyp.MainObject;
import us.hhbz.fyp.communicators.ArduinoCommunicator;
import us.hhbz.fyp.communicators.DummyCommunicator;
import us.hhbz.fyp.enums.AudioTypes;
/**
 * Manages the data transfer between master & the audio Slave
 * Provides java recognized functions instead of arduino defined commands
 * so inter-communication is easier
 * @author Abdul Hadi
 *
 */
public class AudioManager {
	@SuppressWarnings("unused")
	private MainObject main;
	private ArduinoCommunicator communicator=new ArduinoCommunicator("/dev/ttyACM1");
	//private DummyCommunicator communicator=new DummyCommunicator("/dev/ttyACM1");
	public AudioManager(MainObject main) {
		this.main=main;
	}
	/**
	 * Stop playing any audio
	 */
	public void stopPlaying() {
		communicator.sendData("stop");
	}
	/**
	 * play an audio type file
	 * @param type to play
	 */
	public void playSound(AudioTypes type) {
		communicator.sendData("play:"+type.getCommand());
	}
	/**
	 * Increase Volume by 10%
	 */
	public void volumeUp() {
		communicator.sendData("volume:up");
	}
	/**
	 * Decrease Volume by 10%
	 */
	public void volumeDown() {
		communicator.sendData("volume:down");
	}
}
