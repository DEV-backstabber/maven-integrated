package us.hhbz.fyp.managers;

import us.hhbz.fyp.MainObject;
import us.hhbz.fyp.ProjectMain;
import us.hhbz.fyp.communicators.MySqlCommunicator;
/**
 * Used to open a Mysql connection & update the database after some delay
 * We dont read data from the database we just update it in time
 * @author Abdul Hadi
 *
 */
public class MySqlManager {
	private MainObject main;
	private MySqlCommunicator communicator=new MySqlCommunicator();
	public MySqlManager(MainObject main) {
		this.main=main;
	}
	public void setup() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				communicator.create(
						ProjectMain.MYSQL_HOST,
						ProjectMain.MYSQL_PORT,
						ProjectMain.MYSQL_DATABASE,
						ProjectMain.MYSQL_USERNAME,
						ProjectMain.MYSQL_PASSWORD);
				sleep(1000);
				while(true) {
					communicator.updateAll(main.getObdManager().getCarData());
					sleep(1000*10);
				}
				
			}
			private void sleep(int time) {
				try {
					Thread.sleep(time);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
