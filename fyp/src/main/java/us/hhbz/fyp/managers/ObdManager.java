package us.hhbz.fyp.managers;


import us.hhbz.fyp.MainObject;
import us.hhbz.fyp.communicators.ArduinoCommunicator;
import us.hhbz.fyp.communicators.DummyCommunicator;
import us.hhbz.fyp.datastores.CarData;
import us.hhbz.fyp.enums.DataTypes;
/**
 * Used to constantly fetch car's data from obd slave & save it within itself
 * other classes can get that data by using getCarData() method
 * @author Abdul Hadi
 *
 */
public class ObdManager {
	@SuppressWarnings("unused")
	private MainObject main;
	private CarData carData=new CarData();
	private ArduinoCommunicator communicator=new ArduinoCommunicator("/dev/ttyACM0");
	//private DummyCommunicator communicator=new DummyCommunicator("/dev/ttyACM0");
	public ObdManager(MainObject main) {
		this.main=main;
	}
	public void setup() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					requestAllData();
					sleep(1000);
				}
			}
			private void requestAllData() {
				communicator.sendData("reboot");
				sleep(100);
				for(DataTypes type:DataTypes.values()) {
					communicator.sendData(type.getObdCommand());
					sleep(1000);
					carData.setData(type, getInt(communicator.getLastData().replace(type.getObdCommand()+":", "")));
				}
			}

			private int getInt(String data) {
				int i=0;
				try {
					i=Integer.valueOf(data);
				} catch(NullPointerException | NumberFormatException e) {
					i=-1;
				}
				return i;
			}

			private void sleep(int i) {
				try {
					Thread.sleep(i);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	public CarData getCarData() {
		return this.carData;
	}
}
