package us.hhbz.fyp.managers;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;

import us.hhbz.fyp.MainObject;
import us.hhbz.fyp.datastores.CarData;
import us.hhbz.fyp.enums.AudioTypes;
import us.hhbz.fyp.enums.DataTypes;
import us.hhbz.fyp.guis.AbstractGui;
import us.hhbz.fyp.guis.DataScreen;
/**
 * This manager checks for all the conditions predefined by us
 * once checked the condition it stores them in a map
 * it also changes the gui accordingly
 * it also send audio commands by making use of the audiomanager
 * @author Abdul Hadi
 *
 */
public class ConditionManager {
	private MainObject main;
	private Map<DataTypes, DataCondition> condition=new HashMap<DataTypes, ConditionManager.DataCondition>();
	public ConditionManager(MainObject main) {
		this.main=main;
	}
	public void setup () {
		main.getAudioManager().playSound(AudioTypes.WELCOME_FINE);
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(1000*30);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					playSounds();
				}
			}
			private void playSounds() {
				boolean fine=true;
				AudioManager manager=main.getAudioManager();
				for(DataTypes type:condition.keySet()) {
					if(condition.get(type).equals(DataCondition.FAULTY)) {
						switch (type) {
						case RPM:
							manager.playSound(AudioTypes.FAULT_RPM);
							break;

						case SPEED:
							manager.playSound(AudioTypes.FAULT_SPEED);
							break;

						case COOLANT_TEMPERATURE:
							manager.playSound(AudioTypes.FAULT_COOLANT_HOT);
							break;

						case INTAKE_TEMPERATURE:
							manager.playSound(AudioTypes.FAULT_INTAKE_HOT);
							break;

						case ENGINE_TEMPERATURE:
							manager.playSound(AudioTypes.FAULT_ENGINE_HOT);
							break;

						case FUEL_PRESSURE:
							manager.playSound(AudioTypes.FAULT_FUEL);
							break;

						default:
							break;
						}
						fine=false;
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				if(fine)
					manager.playSound(AudioTypes.FINE);
			}
		}).start();
		new Thread(new Runnable() {
			
			@Override
			public void run() 
			{
				while(true) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					updateData();
				}
			}

			

			private void updateData() {
				CarData data=main.getObdManager().getCarData();
				for(DataTypes type:DataTypes.values()) {
					if(type.isInLimit(data.getData(type))) {
						condition.put(type, DataCondition.FINE);
					}
					else {
						condition.put(type, DataCondition.FAULTY);
					}
				}
				AbstractGui gui=main.getRunningGui();
				Map<DataTypes, JButton> buttons = gui.getButtons();
				if(gui instanceof DataScreen) {
					for(DataTypes type:buttons.keySet()) {
						JButton button=buttons.get(type);
						button.setText(type.getDisplayName()+":"+data.getData(type));
						switch (condition.get(type)) {
						case FINE:
							button.setBackground(Color.GREEN);
							button.setForeground(Color.BLACK);
							break;

						case FAULTY:
							button.setBackground(Color.RED);
							button.setForeground(Color.BLACK);
							break;
							
						default:
							break;
						}
					}
				}
			}
		}).start();
	}
	
	
	
	public enum DataCondition {
		FINE,
		FAULTY;
	}
}
